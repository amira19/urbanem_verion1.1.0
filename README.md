# README #

Dépôt git du site principal d’[Urbanem](www.urbanem.com).

Application Wordpress, voir la [documentation](http://codex.wordpress.org).


## Thème installé

Thème de base : [OneEngine](http://www.enginethemes.com/themes/oneengine/), voir aussi [la documentation](https://github.com/syamilmj/Aqua-Page-Builder/wiki) du « Aqua Page Builder » inclus dans le thème.


## Extensions installées

- [BackupWorpdress](https://wordpress.org/plugins/backupwordpress/)
- [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
- [Contact Form 7 Dynamic Text Extension](https://wordpress.org/plugins/contact-form-7-dynamic-text-extension/)
- [Google Analytics Dashboard for WP](https://wordpress.org/plugins/google-analytics-dashboard-for-wp/)
- [MailPoet Newsletters](https://wordpress.org/plugins/wysija-newsletters/)
- [Tabby Responsive Tabs](https://wordpress.org/plugins/tabby-responsive-tabs/)
- [TinyMCE Advanced](https://wordpress.org/plugins/tinymce-advanced/)
- [SEO WordPress](https://wordpress.org/plugins/wordpress-seo/)
- [Advanced Custom Fields](https://wordpress.org/plugins/advanced-custom-fields/) voir la [documentation](http://www.advancedcustomfields.com/resources/)
- [Date and Time Picker Field](https://wordpress.org/plugins/acf-field-date-time-picker/), pensez à vérifier que l’option _“Get field as timestamp”_ est cochée dans les paramètres d’ACF.
- [Contact Form to Email](https://wordpress.org/plugins/contact-form-to-email/)
