<?php
/*
Template Name: Formateurs
*/
setlocale(LC_TIME, "fr_FR");

get_header();

?>

<?php include_once 'includes/sharing-urls.php'; ?>

<div class="clearfix"></div>  

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->

<!-- Container -->

<div class="container">
    <div class="row">
        <div class="single-blog-desktop">
            <div class="col-md-1 col-sm-1 et-post-data-left single-blog">
                <a href="<?php echo home_url(); ?>" class="home-icon"><i class="fa fa-home"></i></a>
                <span class="et-post-date"></span>
                <a href="#" data-id="<?php echo $post->ID; ?>" class="et-like-post <?php echo is_like_post($post->ID); ?>">
                    <span class="et-post-heart"><i class="fa fa-heart"></i><span class="count"><?php echo get_post_meta($post->ID, 'et_like_count', true) ? get_post_meta($post->ID, 'et_like_count', true) : 0; ?></span></span>
                </a>
            </div>
            <div class="col-md-1 col-sm-1">
                <div class="social-share single-blog-share">
                    <ul class="social">
                        <?php 
                            echo $share_buttons;
                        ?> 
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-10 formateurs" <?php post_class(); ?>>

            <h1 class="title-single"><?php the_title(); ?></h1>
            <div class="post-content">
                
                <ul>
                <?php

                    // Posts de la catégorie agenda par date de formation
                    $args = array(
                        'numberposts'   => -1, 
                        'category_name' => 'formateurs',
                        'orderby'       => 'title',
                        'order'         => ASC
                    );
                    $formateurs = get_posts($args);

                    foreach ($formateurs as $formateur) {
                        if (has_post_thumbnail($formateur->ID)) {
                            $thumb = get_the_post_thumbnail($formateur->ID);
                        } else {
                            $placeholderPath  = get_stylesheet_directory_uri() . '/images/profile-pic-placeholder.png';
                            $thumb = '<img src="' . $placeholderPath . '" alt="Pas de photo pour ce formateur">';
                        }
                        ?>
                        <li class="formateur-wrapper">
                            <div class="image-team-wrapper">
                                <?= $thumb; ?>
                            </div>
                            <h4 class="title"><?= $formateur->post_title; ?></h4>
                            <p><?= $formateur->post_content; ?></p>
                        </li>
                        <?php
                    }

                ?>
                </ul>
            </div><!-- Content / End --> 
            
        </div>      
    </div>
</div>

<?php 
    $color_title     = oneengine_option('header_blog_title_color'); 
    $color_sub_title = oneengine_option('header_blog_subtitle_color');
    
    $color_title     = ( ! empty ( $color_title ) )      ? 'color:'. $color_title .';' : '';
    $color_sub_title = ( ! empty ( $color_sub_title ) )  ? 'color:'. $color_sub_title .';' : '';
    
    /** Style Container */
    $title_color = ( 
        ! empty( $color_title ) ) ? 
            sprintf( '%s', $color_title) : '';
    $css_title_color = '';
    if ( ! empty( $title_color ) ) {            
        $css_title_color = 'style="'. $title_color .'" ';
    }
    
    $sub_title_color = ( 
        ! empty( $color_sub_title ) ) ? 
            sprintf( '%s', $color_sub_title) : '';
    $css_sub_title_color = '';
    if ( ! empty( $sub_title_color ) ) {            
        $css_sub_title_color = 'style="'. $sub_title_color .'" ';
    }
?>
<!-- Container / End -->
<?php get_footer(); ?>