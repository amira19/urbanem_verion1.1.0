<?php

/** A simple agenda block **/

// AGENDA BLOCK
if( ! class_exists( 'Agenda_Block' ) ) :

class Agenda_Block extends AQ_Block {

    // set and create block
    function __construct() {
        $block_options = array(
            'name' => 'Agenda',
            'size' => 'span12',
        );

        // create the block
        parent::__construct('Agenda_Block', $block_options);
    }

    function form($instance) {

        $defaults = array(
            'text' => '',
        );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
    }

    function block($instance)
    {
        extract($instance);

        $time = current_time('timestamp');

		$args = array(
            'numberposts'   => 3,
            'post_status'   => 'publish', // only show published events 
            'category_name' => 'agenda', // only for agenda events
            'orderby'       => 'meta_value', // order by date_de_formation
            'meta_key'      => 'date_de_formation', // ACF Date & Time Picker field
            'order'         => 'ASC', // Show earlier events first
            'meta_value'    => $time, // Use the current time from above
            'meta_compare'  => '>=',  // Compare today's datetime with our event datetime
		);
		$formations = get_posts($args);

        $output = "";
        foreach ($formations as $formation) {
        	// Récupération du timestamp pour la date de formation
            $ts     = get_field('date_de_formation', $formation->ID);
            $date   = strftime('%A %d %B %G', $ts);
            $url    = get_permalink($formation->ID);
            $title  = get_the_title($formation->ID);
            $lieu   = get_field('lieu', $formation->ID);
            $region = get_field('region', $formation->ID);

        	$output .='
        		<div class="col-md-4 col-sm-12 formation-block">
                    <div class="service-wrapper formation-wrapper">
                        <h4 class="title"><a href="' . $url . '">' . $title . '</a></h4>
                        <p class="text-center">Date : le ' . $date . '</p>
                        <p class="text-center">Région : '. $region .'</p>
                        <p class="text-center">Lieu : '. $lieu .'</p>
                    </div>
        		</div>
        	';
        }
        if (isset($output) and !empty($output)) {
			echo $output;
		} else {
			echo "Aucune formation n’est prévue pour le moment.";
		}
    }
}
aq_register_block('Agenda_Block');

endif;