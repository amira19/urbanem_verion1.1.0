<?php
/*
Template Name: Calendrier
*/

get_header();

?>

<?php include_once 'includes/sharing-urls.php'; ?>

<div class="clearfix"></div>  

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->

<!-- Container -->

<div class="container">
    <div class="row">
        <div class="single-blog-desktop">
            <div class="col-md-1 col-sm-1 et-post-data-left single-blog">
                <a href="<?php echo home_url(); ?>" class="home-icon"><i class="fa fa-home"></i></a>
                <span class="et-post-date"></span>
                <a href="#" data-id="<?php echo $post->ID; ?>" class="et-like-post <?php echo is_like_post($post->ID); ?>">
                    <span class="et-post-heart"><i class="fa fa-heart"></i><span class="count"><?php echo get_post_meta($post->ID, 'et_like_count', true) ? get_post_meta($post->ID, 'et_like_count', true) : 0; ?></span></span>
                </a>
            </div>
            <div class="col-md-1 col-sm-1">
                <div class="social-share single-blog-share">
                    <ul class="social">
                        <?php 
                            echo $share_buttons;
                        ?> 
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-10 calendrier" <?php post_class(); ?>>

            <h1 class="title-single"><?php the_title(); ?></h1>
            <div class="post-content">

                <?php

                    // Posts de la catégorie agenda par date de formation
                    // Le champ date_de_formation est un custom field 
                    $args = array(
                        'numberposts'   => -1, 
                        'category_name' => 'agenda'
                    );
                    $formations = get_posts($args);

                    // Création d’un tableau des évenements par date
                    $events = array();
                    foreach ($formations as $formation) {
                        $ts = get_field('date_de_formation', $formation->ID);
                        // Uniquement pour les évenements à venir
                        if ($ts > time()) $events[$formation->ID] = $ts;                        
                    }
                    // Trie les évenements par date en préservant les identifiants
                    asort($events);

                    // Affichage
                    $currentMonth = '';
                    $currentYear  = '';
                    foreach ($events as $id => $ts) {
                        $year  = date('Y', $ts);
                        $month = ucfirst(strftime("%B", $ts));

                        // On stocke et affiche l’année en cours si changement
                        if ($year != $currentYear) {
                            $currentYear = $year;
                            echo '<h2 class="year">' . $year . '</h2>';
                        }

                        // On stocke et affiche le mois en cours si changement
                        if ($month != $currentMonth) {
                            $currentMonth = $month;
                            echo '<h3 class="month">' . $month . '</h3>';
                        }

                        $url    = get_permalink($id);
                        $title  = get_the_title($id);
                        $date   = strftime('%A %d %B %G', $ts);
                        $lieu   = get_field('lieu', $id);
                        $region = get_field('region', $id);

                        // Infos de la fiche associée
                        $fiche = get_field('fiche_catalogue', $id);
                        $thumb = get_the_post_thumbnail($fiche->ID);
                        $duree = get_field('duree', $fiche->ID);

                        echo '
                        <div class="formation-wrapper">
                            <div class="thumb">'. $thumb .'</div>
                            <h4 class="title"><a href="' . $url . '">' . $title . '</a></h4>
                            <p>Date : le ' . $date . '. Durée : ' . $duree . '.</p>
                            <p>Région : '. $region .'</p>
                            <p>Lieu : '. $lieu .'</p>
                        </div>
                        ';
                    }


                ?>

            </div><!-- .post-content --> 
        </div><!-- .calendrier -->
    </div><!-- .row -->
</div><!-- .container -->

<?php 
    $color_title     = oneengine_option('header_blog_title_color'); 
    $color_sub_title = oneengine_option('header_blog_subtitle_color');
    
    $color_title     = ( ! empty ( $color_title ) )      ? 'color:'. $color_title .';' : '';
    $color_sub_title = ( ! empty ( $color_sub_title ) )  ? 'color:'. $color_sub_title .';' : '';
    
    /** Style Container */
    $title_color = ( 
        ! empty( $color_title ) ) ? 
            sprintf( '%s', $color_title) : '';
    $css_title_color = '';
    if ( ! empty( $title_color ) ) {            
        $css_title_color = 'style="'. $title_color .'" ';
    }
    
    $sub_title_color = ( 
        ! empty( $color_sub_title ) ) ? 
            sprintf( '%s', $color_sub_title) : '';
    $css_sub_title_color = '';
    if ( ! empty( $sub_title_color ) ) {            
        $css_sub_title_color = 'style="'. $sub_title_color .'" ';
    }
?>
<!-- Container / End -->
<?php get_footer(); ?>