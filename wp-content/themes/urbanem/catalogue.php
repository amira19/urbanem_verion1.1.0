<?php
/*
Template Name: Catalogue
*/
?>

<?php 
global $post,$wp_query; 
get_header();
the_post();
?>   


<div class="clearfix"></div>      

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->

<!-- Container -->

<div class="container">
    <div class="row">

        <div class="col-md-12" <?php post_class(); ?>>

            <div class="post-content catalogue-page">
                <?php the_content(); ?>
            </div><!-- Content / End -->
            <?php 
                if( has_tag() ) {
                    the_tags('<div class="post-tags">', ', ', '</div><!-- Tags / End -->');
                }
            ?>    
            
        </div>      
    </div>
</div>

<!-- Container / End -->
<?php get_footer(); ?>