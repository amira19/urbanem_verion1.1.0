<?php
global $post,$wp_query;
get_header();
the_post();

// Si page formation on récupère la fiche catalogue correspondante
$fiche = get_field('fiche_catalogue');

?>

<?php include_once 'includes/sharing-urls.php'; ?>

<div class="clearfix"></div>

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->

<!-- Container -->

<div class="container">
	<div class="row">
        <!-- Social buttons -->
    	<div class="single-blog-desktop col-md-3 col-sm-3">
            <div class="et-post-data-left single-blog side-buttons-home">
                <a href="<?php echo home_url(); ?>" class="home-icon"><i class="fa fa-home"></i></a>
                <?php

                if (isset($fiche) and $fiche != null and $fiche != false) :
                    $month = date('M', get_field('date_de_formation'));
                    $day   = date('d', get_field('date_de_formation'));
                ?>
                    <span class="et-post-month"><?= $month; ?></span>
                    <span class="et-post-date"><?= $day; ?></span>
                <?php else : ?>
                    <span class="et-post-date"></span>
                <?php endif; ?>
                <a href="#" data-id="<?php echo $post->ID; ?>" class="et-like-post <?php echo is_like_post($post->ID); ?>">
                    <span class="et-post-heart"><i class="fa fa-heart"></i><span class="count"><?php echo get_post_meta($post->ID, 'et_like_count', true) ? get_post_meta($post->ID, 'et_like_count', true) : 0; ?></span></span>
                </a>
            </div>
            <div class="social-share single-blog-share side-buttons-home">
                <ul class="social">
                    <?php
                        echo $share_buttons;
                    ?>
                </ul>
            </div>

            <?php if (isset($fiche) and $fiche != null) : ?>

                <p class="location">Lieu : <strong><?php echo get_field('lieu'); ?></strong></p>

                <h2 class="btn-inscription"><a href="#inscription-wrapper" class="btn-show-form">S’inscrire</a></h2>
            <?php endif; ?>

            <div class="formations-similaires">
                <h3>Formations similaires</h3><br/>
                <?php
                // S’il s’agit d’une formation on récupère les catégories de la fiche correspondante
                if (isset($fiche) and $fiche != null) {
                    $categories = get_the_category($fiche->ID);
                } else {
                    global $post;
                    $categories = get_the_category(); //get all categories for this post
                }

                $cat_ID = array();
                foreach ($categories as $category) {
                    array_push($cat_ID, $category->cat_ID);
                }
                $args = array(
                    'orderby'      => 'date',
                    'order'        => 'DESC',
                    'post_type'    => 'post',
                    'numberposts'  => 8,
                    'post__not_in' => array($post->ID),
                    'category__in' => $cat_ID
                ); // post__not_in will exclude the post we are displaying

                $cat_posts = get_posts($args);
                $out = '';
                foreach ($cat_posts as $cat_post) {
                    $out .= '<li>';
                    $out .=  '<a href="'.get_permalink($cat_post->ID).'" title="'.wptexturize($cat_post->post_title).'">'.wptexturize($cat_post->post_title).'</a></li>';
                }
                $out = '<ul class="cat_post">' . $out . '</ul>';
                echo $out;

                ?>
            </div>

        </div>

        <!-- Content -->
		<div class="col-md-9 content-container" <?php post_class(); ?>>

			<div class="post-content">

                <h1 class="title-single"><?php the_title(); ?></h1>

                <?php the_content(); ?>

                <?php

                // Si une fiche catalogue est spécifiée
                // affichage de la fiche catalogue correspondante
                if (isset($fiche) and $fiche != null) {
                    echo $fiche->post_content;
                }
                ?>

			</div><!-- Content / End -->

		</div>
	</div>
</div>

<?php
// Si une fiche formateur est spécifiée
// affichage de la fiche formateur correspondante
$formateurs = get_field('intervenant');

if (isset($formateurs) and $formateurs != null) :

    if (is_array($formateurs)) {
        foreach ($formateurs as $formateur) {
            $plural  = 's';
            $thumb   = get_the_post_thumbnail($formateur->ID, 'thumbnail', array('class' => 'img-responsive'));
            $title   = $formateur->post_title;
            $content = $formateur->post_content;

            $infos .= '
            <div class="formateur-wrapper">
                <div class="thumb-wrapper">
                    ' . $thumb . '
                </div>
                <div class="intervenant-presentation">
                    <h2>' . $title . '</h2>
                    <span class="position">' . $content . '</span>
                </div>
            </div>
            ';
        }
    } else {
        $thumb   = get_the_post_thumbnail($formateurs->ID, 'full', array('class' => 'img-responsive'));
        $title   = $formateurs->cdpost_title;
        $content = $formateurs->post_content;

        $infos .= '
        <div class="formateur-wrapper">
            <div class="thumb-wrapper">
                ' . $thumb . '
            </div>
            <div class="intervenant-presentation">
                <h2>' . $title . '</h2>
                <span class="position">' . $content . '</span>
            </div>
        </div>
        ';
    }

    ?>
    <!-- Présentation formateur -->
    <div class="aside-formateurs">
        <h1 class="title">Intervenant<?= $plural; ?></h1>

        <?= $infos; ?>

    </div><!-- aside-formateur -->

<?php endif; ?>


<?php

// Si une fiche catalogue est spécifiée
// affichage du formulaire d’inscription
if (isset($fiche) and $fiche != null) {
    if (oneengine_option('contact_form') != '') {
        ?>

        <script>

          jQuery(document).ready(function ($) {
            // Affiche form masqué
            $(".btn-show-form").click(function() {
                $(".heading-title-wrapper ").removeClass("invisible");
                $(".contact-form-wrapper").removeClass("invisible");
                $("#show-form").addClass("invisible");
            });
          });

        </script>

        <div class="inscription-wrapper" id="inscription-wrapper">
            <p class="sub-title">Cette formation vous intéresse ?</p>
            <button id="show-form" class="btn-show-form btn-more">Inscrivez-vous</button>

            <div class="heading-title-wrapper invisible" style="color">
                <h2 class="title" style="color:#ffffff;">Inscrivez-vous</h2>
                <span class="line-title" style="background-color:#fff"></span>
                <span class="sub-title" style="color:#ffffff;">Vous souhaitez participez à cette formation ? Merci de renseigner les champs suivants.</span>
            </div>

            <div class="contact-form-wrapper invisible">
                <h2 class="contact-title"><?php echo __('', 'oneengine')?></h2>
                <?php echo do_shortcode( '[contact-form-7 id="256" title="Inscription formation"]' ); ?>
            </div>
        </div>
        <?php
    }
}
?>

<div class="search-formation-wrapper">
    <?php include_once 'includes/formation-search.php'; ?>
</div>


<!-- Container / End -->
<?php get_footer(); ?>
