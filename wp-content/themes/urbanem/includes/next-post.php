<!-- Formation suivante -->
<?php
	$next_post = get_next_post(true);
	if (empty( $next_post )){
		$post = get_posts(array(
                'post_type'        => 'post',
                'category__not_in' => 18,
                'order'            => 'ASC',
                'posts_per_page'   => 1,
	    		));
		$next_post = $post[0];
	}
?>
<?php 
	$color_title		= oneengine_option('header_blog_title_color'); 
	$color_sub_title	= oneengine_option('header_blog_subtitle_color');
		
	$color_title		= ( ! empty ( $color_title ) ) 		? 'color:'. $color_title .';' : '';
	$color_sub_title	= ( ! empty ( $color_sub_title ) )  ? 'color:'. $color_sub_title .';' : '';
	
	/** Style Container */
	$title_color = ( 
		! empty( $color_title ) ) ? 
			sprintf( '%s', $color_title) : '';
	$css_title_color = '';
	if ( ! empty( $title_color ) ) {			
		$css_title_color = 'style="'. $title_color .'" ';
	}
	
	$sub_title_color = ( 
		! empty( $color_sub_title ) ) ? 
			sprintf( '%s', $color_sub_title) : '';
	$css_sub_title_color = '';
	if ( ! empty( $sub_title_color ) ) {			
		$css_sub_title_color = 'style="'. $sub_title_color .'" ';
	}
?>
<a href="<?php echo get_permalink( $next_post->ID ); ?>">
    <div class="next-post-container" >
        <div class="animation-wrapper col-md-12">
            <div class="heading-title-wrapper blog-page">         
                <p class="title-next">FORMATION SUIVANTE</p>
                <h2 class="title"><?php echo $next_post->post_title ?></h2>
                <span class="line-title"></span>
                <span class="sub-title"><?php echo $next_post->post_excerpt; ?></span>
            </div>
            <div class="btn-more" style="color">         
                <p class="title-more">VOIR LA FICHE FORMATION</p>
            </div>
        </div>
    </div>
</a>