<?php

$postURL     = get_permalink();
$postTitle   = get_the_title();
$postSummary = get_the_excerpt();

// Get sharing options
$sharing_sites  = oneengine_option('social_share_blog');

$twitter_share	= '';
$fb_share 		= '';
$google_share 	= '';

foreach ( $sharing_sites as $key => $value ) {

	// Twitter
	if ( $key == 'twitter' && $value ) {
		$twitter_share = '<li><a class="sb-twitter" href="http://twitter.com/share?url=' . $postURL . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600\');return false;" data-count="none" data-via=" " title="Partager sur Twitter"><i class="fa fa-twitter"></i></a></li>';
	}

	// Facebook
	if ( $key == 'facebook' && $value ) {
		$fb_share = '<li><a class="sb-facebook" href="http://www.facebook.com/sharer/sharer.php?u=' . $postURL . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=660\');return false;" target="_blank" title="Partager sur Facebook"><i class="fa fa-facebook"></i></a></li>';
	}

	// Google+
	if ( $key == 'google_plus' && $value ) {
		$google_share = '<li><a class="sb-google" href="https://plus.google.com/share?url=' . $postURL . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500\');return false;" title="Partager sur Google+"><i class="fa fa-google-plus"></i></a></li>';
	}
}


// Linkedin
$postURLLinkedin = urlencode(home_url() . "?p=" . get_the_ID());
$linkedin_share  = '<li><a class="sb-linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=' . $postURLLinkedin.'&amp;title=' . urlencode($postTitle) . '&amp;source=' . urlencode(home_url()) . '&amp;summary=' . urlencode($postSummary) . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;" title="Partager sur Linkedin"><i class="fa fa-linkedin"></i></a></li>';

// Viadeo
// Voir CSS pour hack affichage bouton
$viadeo_share = '<li><a class="sb-viadeo"  href="http://www.viadeo.com/shareit/share/?url=' . $postURL . '&title=' . urlencode($postTitle) . '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"><img src="http://urbanem.fr/wp-content/themes/urbanem/images/viadeo-button.png"></a></li>';


$share_buttons = $fb_share . $twitter_share . $google_share . $linkedin_share . $viadeo_share;
