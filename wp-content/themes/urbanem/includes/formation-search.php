<!-- Formation suivante -->

<div class="search-formation">
	<h1><span>Recherchez</span> une formation</h1>

	<form action="<?php echo get_page_link(184); ?>" method="post">

		<input type="hidden" name="action" value="search_form">

		<label for="theme">Thème :</label>
		<select name="theme">
			<?php 

			$args = array('child_of' => 16);
			$cats = get_categories($args);

		    foreach ($cats as $cat) {
				echo '<option value="' . $cat->cat_ID . '">' . $cat->name . '</option>';
			}

			?>
		</select>

		<label for="region">Région :</label>
		<select name="region">
			<?php 

			// See http://wordpress.stackexchange.com/questions/47031/show-values-of-custom-field-created-with-acf-on-a-page

			$fieldKey = 'field_542d083d7109d';
			$regions  = get_field_object($fieldKey);

			foreach ($regions['choices'] as $key => $value) {
				if ($value === 'RHONE-ALPES') {
					echo '<option selected value="' . $key . '">' . $value . '</option>';
				} else {
					echo '<option value="' . $key . '">' . $value . '</option>';
				}
			}

			?>
		</select>
		
		<button type="submit" class="btn">Rechercher</button>

	</form>
</div>
