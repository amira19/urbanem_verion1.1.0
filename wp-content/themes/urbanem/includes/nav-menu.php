<header id="header" class="row">
    
    <div class="btn-top"><a href="#top"><img src="<?= get_stylesheet_directory_uri();; ?>/images/arrow_top.png" alt="Remonter en haut de la page"></a></div>

    <div class="col-md-3 col-xs-3 logo-wrapper">

        <a href="<?php echo home_url(); ?>">
        <?php 
            $top   = '' ;
            $left  = '' ;
            $width = '' ;
            if( oneengine_option('logo_top') != '' )$top    = 'top:'.oneengine_option('logo_top').'px;' ;
            if( oneengine_option('logo_left') != '' )$left  = 'left:'.oneengine_option('logo_left').'px;';
            if( oneengine_option('logo_width') != '' )$width = 'width:'.oneengine_option('logo_width').'px;';
            if( oneengine_option('custom_logo', false, 'url') !== '' ){
                echo '<img src="'. oneengine_option('custom_logo', false, 'url') .'" alt="'.get_bloginfo( 'name' ).' logo" />';
            } else {
        ?>
            <div class="logo-img"><span>E</span></div>
        <?php } ?>
        </a>

    </div>
    
    <div class="col-md-9 col-xs-9 menu-top-wrapper">

        <script>
            jQuery(document).ready(function($) {
                $('#menu-lateral-menu').slicknav({
                    prependTo:'.menu-responsive'
                });
            });
        </script>

        <!-- Menu --> 
        <nav id="lateral-menu">
             <?php 
                wp_nav_menu( array( 
                    'theme_location'  => 'lateral' ,
                    'container_class' => 'menu',
                    'menu_class'      => 'lat-menu'
                )); 
            ?>
        </nav>
        <!-- Menu / End -->
        
        <nav class="menu-responsive"> 
        </nav>
    </div>
</header><!-- Header / End -->