<?php

/** A simple texte block **/

// AGENDA BLOCK
if( ! class_exists( 'Texte_Block' ) ) :

class Texte_Block extends AQ_Block {

    // set and create block
    function __construct() {
        $block_options = array(
            'name' => 'Texte',
            'size' => 'span12',
        );

        // create the block
        parent::__construct('Texte_Block', $block_options);
    }

    function form($instance) {

        $defaults = array(
            'text' => '',
        );
        $instance = wp_parse_args($instance, $defaults);
        extract($instance);
    }

    function block($instance)
    {
        extract($instance);

        $args = array(
            'numberposts' => 1, 
            'tag'         => 'une'
        );
        $fiches = get_posts($args);

        foreach ($fiches as $fiche) {
            setup_postdata($fiche);
            $year = mysql2date("Y", $fiche->post_date_gmt);
            $content = $fiche->post_content;
            $excerpt = custom_trim_words($content, '35');

            $output .='
                <div class="detail-wrapper ">
                    <div class="line-wrapper">
                        <span class="line-horizal"></span>
                        <span class="line-vertical"></span>
                        <span class="line-circle">'.$year.'</span>
                    </div>
                    <div class="text-wrapper">
                        <div class="detail">
                            <h2><a href="' . get_permalink($fiche->ID) . '">' . get_the_title($fiche->ID) . '</a></h2>
                            <p>'.$excerpt.'</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="animation-wrapper" style="text-align:center; margin:5px 0 30px"><a target="_self" href="' . get_permalink($fiche->ID) . '" class="btn btn-oe ">DÉCOUVRIR NOTRE NOUVELLE FORMATION</a>
                    </p>
                </div>
            ';
        }
        if (isset($output) and !empty($output)) {
            echo $output;
        } else {
            echo '
            <div class="col-md-12">
                <p class="text-center">Aucune formation n’est prévue pour le moment.</p>
            </div>
            ';
        }
    }
}
aq_register_block('Texte_Block');

endif;