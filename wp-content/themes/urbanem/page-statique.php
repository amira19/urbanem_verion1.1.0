<?php
/*
Template Name: Page statique
*/
?>

<?php 
global $post,$wp_query; 
get_header();
the_post();
?>   
<?php include_once 'includes/sharing-urls.php'; ?>

<div class="clearfix"></div>      

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->


<!-- Container -->

<div class="container">
    <div class="row">
        <div class="single-blog-desktop">
            <div class="col-md-1 hidden-sm et-post-data-left single-blog">
                <a href="<?php echo home_url(); ?>" class="home-icon"><i class="fa fa-home"></i></a>
                <span class="et-post-date"></span>
                <a href="#" data-id="<?php echo $post->ID; ?>" class="et-like-post <?php echo is_like_post($post->ID); ?>">
                    <span class="et-post-heart"><i class="fa fa-heart"></i><span class="count"><?php echo get_post_meta($post->ID, 'et_like_count', true) ? get_post_meta($post->ID, 'et_like_count', true) : 0; ?></span></span>
                </a>
            </div>
            <div class="col-md-1 hidden-sm">
                <div class="social-share single-blog-share">
                    <ul class="social">
                        <?php 
                            echo $share_buttons;
                        ?> 
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-10 col-sm-12" <?php post_class(); ?>>

            <h1 class="title-single"><?php the_title(); ?></h1>

            <div class="post-content">
                <?php the_content(); ?>
            </div><!-- Content / End --> 
            
        </div>      
    </div>
</div>

<div class="search-formation-wrapper">
    <?php include_once 'includes/formation-search.php'; ?>
</div>

<!-- Container / End -->
<?php get_footer(); ?>