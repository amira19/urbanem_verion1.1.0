<?php
/*
Template Name: Recherche
*/
?>

<?php
get_header();

?>     
<?php include_once 'includes/sharing-urls.php'; ?>

<div class="clearfix"></div>   

<!-- Header -->
<?php include_once 'includes/nav-menu.php'; ?>
<!-- Header / End -->

<!-- Container -->

<div class="container">
    <div class="row">
        
        <div class="single-blog-desktop">
            <div class="col-md-1 col-sm-1 et-post-data-left single-blog">
                <a href="<?php echo home_url(); ?>" class="home-icon"><i class="fa fa-home"></i></a>
                <span class="et-post-date"></span>
                <a href="#" data-id="<?php echo $post->ID; ?>" class="et-like-post <?php echo is_like_post($post->ID); ?>">
                    <span class="et-post-heart"><i class="fa fa-heart"></i><span class="count"><?php echo get_post_meta($post->ID, 'et_like_count', true) ? get_post_meta($post->ID, 'et_like_count', true) : 0; ?></span></span>
                </a>
            </div>
            <div class="col-md-1 col-sm-1">
                <div class="social-share single-blog-share">
                    <ul class="social">
                        <?php 
                            echo $share_buttons;
                        ?> 
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-10 recherche-wrapper">

            <h1 class="title-single">Recherche de formations</h1>

            <div>

                <!-- Liste des posts correspondant à la recherche -->
                <div class="col-md-12">
                    <ul>
                    <?php 

                    $results = getSearchResults();
                    $counter = 0;
                    if ($results == false) {
                        $counter = 0;
                    } else {
                        $query = $results['query'];
                        if ($query->have_posts()) {
                            while ($query->have_posts()) : $query->the_post();

                                // Identifiant du post formation
                                $id = get_the_ID();
                                
                                // Infos de la fiche catalgoque correspondante
                                $fiche = get_field('fiche_catalogue');
                                // S’il existe une fiche
                                if (isset($fiche) and $fiche != null) {
                                    // On récupère les catégories de cette fiche
                                    $cats = get_the_category($fiche->ID);
                                    foreach ($cats as $cat) {
                                        // On les compare au thème indiqué par la recherche
                                        // Si la fiche est dans une catégorie correspondante
                                        // On affiche les infos du post formation 
                                        // (non pas de la fiche catalogue)
                                        if ($cat->cat_ID == $results['theme']) {
                                            $url    = get_permalink($id);
                                            $title  = get_the_title($id);
                                            $date   = date('d/m/Y', get_field('date_de_formation'));
                                            $hour   = date('H\hm', get_field('date_de_formation'));
                                            $lieu   = get_field('lieu');
                                            $region = get_field('region');
                                            $thumb  = get_the_post_thumbnail($fiche->ID);
                                            $counter++;
                                            
                                            ?>
                                            <li class="formation-wrapper">
                                                <div class="thumb"><?= $thumb; ?></div>
                                                <h4 class="title"><a href="<?= $url ?>"><?= $title; ?></a></h4>
                                                <p>Date : le <?= $date; ?> à <?= $hour; ?></p>
                                                <p>Région : <?= $region; ?></p>
                                                <p>Lieu : <?= $lieu; ?></p>
                                            </li>
                                            <?php
                                        }
                                    }
                                } 

                            endwhile;
                        }
                    }
                    if ($counter == 0) {
                        echo 'Aucune formation ne correspond à vos critères de recherches.';
                    }

                    ?>
                    </ul>
                </div>
                
            </div><!-- Content / End -->   
            
        </div>      
    </div>
</div>

<!-- Container / End -->
<?php get_footer(); ?>